# NI-MVI - Semestrální práce - Daniel Pilař

## Použité knihovny

* Python 3.10
* pandas
* numpy
* sklearn
* tslearn
* torch
* matplotlib
* scipy
* jupyter

Notebooky lze spustit po nainstalování potřebných balíčků v prostředí Jupyter Notebook.

## Popis jednotlivých souborů

* *DataExploration.ipynb* - Jupyter notebook obsahující prvotní exploraci a základní vizualizaci vstupních dat
* *MyPrecTimeModel.ipynb* - neúspěšný pokus o implementaci modelu, který je v milestone reportu zmíněn jako druhý
* *SimpleSeq2SeqModel.ipynb* - dosavadní nejlepší řešení, předzpracování časových řad (uložení do Datasetu, DataLoaderu, padding, atd) a jednodušší model, taktéž uvedený v milestone reportu. - **Hlavní notebook**
* *test_series.parquet* - část vstupních dat, je výrazně menší než trénovací data, proto je v repozitáři ponechána, slouží pro submission na Kaggle
* *sample_submission.csv* - ukázkový soubor, jak by měla vypadat submission
* *milestone-report.pdf* - průběžný report pro milestone
* *report.pdf* - finální report
* soubory končící na *.model* - uložené state dict průběžných modelů, exportovány z hlavního notebooku

## Další důležité soubory, které pro svou velikost nejsou zahrnuty v prepozitáři

* *train_series.parquet* - vstupní časové řady pro trénování
* *train_events.csv* - popis změn stavů v časových řadách

Oba tyto soubory lze stáhnout přímo u příslušné Kaggle [soutěže](https://www.kaggle.com/competitions/child-mind-institute-detect-sleep-states/data).
